'use strict';
var module = angular.module("avengers", ['ngRoute']);

var conversations;

module.controller("ConversationController", function($scope,$http) {
    $http.get('http://localhost:1337/conversation').
        success(function(data) {
            $scope.conversations = data;
            if (!conversations) {
                conversations = {};
                for(var i in $scope.conversations) {
                    conversations[$scope.conversations[i].conversationId] = $scope.conversations[i];
                }
            }
        });
});

module.controller("MessageController", function($scope,$http,$routeParams) {
    $http.get('http://localhost:1337/conversation/' + $routeParams.conversationId + '/messages').
        success(function(messages) {
            $scope.messages = messages;
            $scope.conversation = conversations[$routeParams.conversationId];
        });
});

module.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: '../templates/conversation.html',
                controller: 'ConversationController'
            }).
            when('/conversation/:conversationId', {
                templateUrl: '../templates/message.html',
                controller: 'MessageController'
            }).
            otherwise({
                redirectTo: ''
            });
    }]);

var injector = angular.injector(['ng', 'avengers']);