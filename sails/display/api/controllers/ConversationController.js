/**
 * ConversationController
 *
 * @description :: Server-side logic for managing conversations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Conversations = require('../models/Conversations');

module.exports = {
	allConversations: function(req, res) {
		// res.status(200).json('conversations');
		Conversations.findAll(function(conversations) {
			res.status(200).json(conversations);
		});
	}
};

