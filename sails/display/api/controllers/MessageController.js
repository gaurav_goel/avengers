/**
 * MessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Messages = require('../models/Messages');
module.exports = {
	findByConversationId: function(req, res) {
		var conversationId = req.params['conversationId'];
		if (!conversationId) {
			res.status(400).send('Conversation Id is mandatory.');
		}
		Messages.findByConversationId(conversationId, function(messages) {
			res.status(200).send(messages);
		});
	}
};

