/**
* Villians.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	villianId: String,
    name: String, 
    description: String, 
    imageURL: String, 
    dangerRank: Number,
    status: String
  }
};

