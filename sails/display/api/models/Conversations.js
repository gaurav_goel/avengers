/**
* Conversation.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	conversationId: String,
	timestamp: Date,
	sender: String,
  	receiver: String,
  	villian: {
  		model:'villians'
  	},
  },

  findAll: function(callback) {
  	Conversations.find().populate('villian').exec(function(err, conversations) {
		if (err) {
			callback([]);
		} else {
			callback(conversations);
		}
	});
  }
};

