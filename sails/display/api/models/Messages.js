/**
* Message.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	conversationId: String,
	timestamp: Date,
	sender: String,
  	receiver: String,
	message: String
  },

  findByConversationId: function(conversationId, callback) {
  	Messages.find({
  		conversationId: conversationId
  	}).sort('timestamp', 'ascending').exec(function(err, messages) {
  		if (err) {
  			console.error(err);
  			callback([]);
  		} else {
  			callback(messages);
  		}
  	});
  }
};

