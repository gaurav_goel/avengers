var mongoose = require('mongoose');

var connect = function (environment) {
  var options = {
      server: {
        socketOptions: { keepAlive: 1 },
        poolSize: 10
      },
      forceServerObjectId: true
    };
  if (mongoose.connection.readyState == 0) {
    mongoose.connect('mongodb://localhost/avengers', options);
  }
  return mongoose;
};
// Error handler
mongoose.connection.on('error', function (err) {
  console.error('Error occurred while connecting to database - ' + err);
});
// Log success message on successful connection creation
mongoose.connection.once('open', function () {
  console.info('Successfully connected to the database.');
});
// Reconnect when closed
mongoose.connection.on('disconnected', function () {
  console.warn('Trying to reconnect to the ');
  connect();
});

////////////////////////////////
// Mongoose Schema
////////////////////////////////
var villianSchema = new mongoose.Schema({
    villianId: String
  , name: String
  , description: String
  , imageURL: String
  , dangerRank: Number
  , status: String
});
var Villian = mongoose.model('Villian', villianSchema);
var conversationSchema = new mongoose.Schema({
  	conversationId: String
	, timestamp: Date
	, sender: String
  , receiver: String
  , villian: {type: mongoose.Schema.Types.ObjectId, ref:'Villian'}
});

var messageSchema = new mongoose.Schema({
  	conversationId: { type: String }
	, timestamp: Date
	, sender: String
  , receiver: String
	, message: String
});
var Conversation = mongoose.model('Conversation', conversationSchema);
var Message = mongoose.model('Message', messageSchema);
////////////////////////////////
// Exports
////////////////////////////////
module.exports = {
  connect: function (env) {
    connect(env);
  },
  models: {
  	Conversation: Conversation,
  	Message: Message
  },
  isConnected: mongoose.connection.readyState == 0
};