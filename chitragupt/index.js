var net = require('net');
var http = require('http');
var imaging = require('imaging');
var color = require('cli-color');
var config = require('./config.js');
var MY_NAME = config.MY_NAME;
var HOST = config.HOST;
var PORT = config.PORT;
var HTTP_PORT = config.HTTP_PORT;

var uuid = require('uuid');
var mongoose = require('./mongoose');
mongoose.connect();
var Conversation = mongoose.models.Conversation;
var Message = mongoose.models.Message;

imaging.draw('./static/log.png', {char: '@'}, function(resp, status) {
        console.log(resp);
});

var saveMessage = function(conversation, sender, message, callback) {
    var message = new Message({conversationId: conversation.conversationId
        , timestamp: new Date()
        , sender: sender
        , receiver: MY_NAME
        , message: message});
    message.save(function(err, message) {
        callback(err, message);
    });
}

var saveOrUpdateConversation = function(conversationId, sender, message, villian, callback) {
    Conversation.find({conversationId: conversationId}).exec(function (err, conversations) {
        if (conversations && conversations.length > 0) {
            var conversation = {conversationId: conversationId};
            saveMessage(conversation, sender, message, function (err, message) {
                    if (err) {
                        console.log(err);
                    } else {
                        callback(conversationId);
                    }
                });
        } else {
            conversationId = uuid.v4();
            var conversation = new Conversation({
                conversationId: conversationId,
                timestamp: new Date(),
                sender: sender,
                villian: villian,
                receiver: MY_NAME
            });
            conversation.save(function(err, conv) {
                conversation.conversationId = conv.conversationId;
                saveMessage(conversation, sender, message, function (err, message) {
                    if (err) {
                        console.log(err);
                    } else {
                        callback(conversationId);
                    }
                });
            });
        }
    });
};

// Create a server instance, and chain the listen function to it
// The function passed to net.createServer() becomes the event handler for the 'connection' event
// The sock object the callback function receives UNIQUE for each connection
net.createServer(function(sock) {    
    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        data = JSON.parse(data);
        console.log(data);
        

        var gotConversationId = function(conversationId) {
            // Write the data back to the socket, the client will receive it as data from the server
            var logMsg = "["+conversationId+"] " + (data.sender ? data.sender: 'Unknown')  +' said, "' + data.message + "";
            switch(data.sender) {
                case 'Captain America': 
                    console.log(color.blue(logMsg));
                    break;
                case 'Iron Man': 
                    console.log(color.red(logMsg));
                    break;
                case 'Hulk': 
                    console.log(color.green(logMsg));
                    break;
                case 'S.H.I.E.L.D.':
                    console.log(color.black.bgWhite(logMsg));
                    break;
                default:
                    console.log(color.orange(logMsg));
            }
            
            console.log(JSON.stringify({
                conversationId: conversationId,
                message: logMsg
            }));
            sock.write(JSON.stringify({
                conversationId: conversationId,
                message: logMsg
            }));
        };

        data.conversationId = saveOrUpdateConversation(data.conversationId, data.sender, data.message, data.villian, gotConversationId);
    });
    
    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        // console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
    
}).listen(PORT, HOST);

var httpServer = http.createServer(function(request, response){
    console.log('request received');
    response.end();
});


httpServer.listen(HTTP_PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("HTTP server is up on: http://localhost:%s", HTTP_PORT);
});

console.log('Server listening on ' + HOST +':'+ PORT);