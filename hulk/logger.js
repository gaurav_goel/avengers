var net = require('net');
var config = require('./config.js');
var MY_NAME = config.MY_NAME;
var HOST = config.HOST;
var PORT = config.LOGGER_PORT;
var sleep = require('sleep');

var sendMessage = function(message, lastConversationId, callback) {
    var client = new net.Socket();
    client.connect(PORT, HOST, function() {
        console.log('CONNECTED TO: ' + HOST + ':' + PORT);
        // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client 
        client.write(JSON.stringify({
            sender: MY_NAME,
            message: message,
            conversationId: lastConversationId
        }));
    });

    client.on('error', function(ex) {
      console.log("Logger failure - ");
      console.log(ex);
      client.destroy();
    });

    // Add a 'data' event handler for the client socket
    // data is what the server sent to this socket
    client.on('data', function(data) {
        console.log('DATA: ' + data);
        // Close the client socket completely
        client.destroy();
    });

    // Add a 'close' event handler for the client socket
    client.on('close', function() {
        callback(lastConversationId);
    });
}


module.exports = {
    sendMessage: sendMessage
}