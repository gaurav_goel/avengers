var net = require('net');
var config = require('./config.js');
var imaging = require('imaging');
var color = require('cli-color');
var MY_NAME = config.MY_NAME;
var HOST = config.HOST;
var PORT = config.PORT;
var logger = require('./logger');

var random = function(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
}

var replies = [
    "Ok, captain with a 'dish'.",
    "Hulk SMASH.",
    "Hulk CRUSH.",
    "HULK IS NOT AFRAID...HULK IS STRONGEST ONE THERE IS!!!",
    "Hulk ANGRY.",
    "Puny human.",
    "Hulk on it.",
    "HULK SMASH AND BASH",
    "Hulk help captain."
];

net.createServer(function(sock) {    
    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        data = JSON.parse(data);
        console.log(data);

        var gotConversationId = function(conversationId) {
            // Write the data back to the socket, the client will receive it as data from the server
            var logMsg = "["+conversationId+"] " + (data.sender ? data.sender: 'Unknown')  +' said, "' + data.message + "";
            switch(data.sender) {
                case 'Chitragupt': 
                    console.log(color.blue(logMsg));
                    break;
                case 'Captain America':
                    console.log(color.blue(logMsg));
                    var msg = "Challenge accepted. " + replies[random(0, replies.length - 1)];
                    sock.write(msg);

                    logger.sendMessage(msg, conversationId, function() {
                        console.log('Assignment accpeted.');
                    });
                    break;
                default:
                    console.log(color.red(log));
            }
        };

        gotConversationId(data.conversationId);
        sock.destroy();
    });
    
    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        sock.destroy();
        // console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
    
}).listen(PORT, HOST);
console.log('Server listening on ' + HOST +':'+ PORT);
imaging.draw('./static/hulk.jpg', {char: '@'}, function(resp, status) {
        console.log(resp);
});