var net = require('net');
var config = require('./config.js');
var imaging = require('imaging');
var color = require('cli-color');
var MY_NAME = config.MY_NAME;
var HOST = config.HOST;
var PORT = config.PORT;
var logger = require('./logger');
var assignment = require('./assignment.js');
var mongoose = require('./mongoose.js');
mongoose.connect();
var uuid = require('uuid');
var sleep = require('sleep');
var sprintf=require("sprintf-js").sprintf;

///////////////////////////////
// Create server
///////////////////////////////
var server = net.createServer(function(socket) {
    socket.write('Echo server\r\n');
    socket.pipe(socket);
});
server.listen(PORT, HOST);
imaging.draw('./static/nickfury.jpg', {char: '@'}, function(resp, status) {
        console.log(resp);
        searchVillians();
});


var Villian = mongoose.models.Villian;

var random = function(low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low);
}

var buildUpSentences = [
		"Assigning Mission to AVENGERS.",
		"City needs help again.",
		"Time to fight crime.",
		"Gosh! Another super villian in town.",
		"What are we expecting next? An army? From outer space?",
		"Thor's hammer nearly broke the Tesseract. LOL.",
		"Do not blow stuff up. I repeat, do not blow stuff up.",
		"Hurry up guys, DC Comics' gaining popularity.",
		"And Tony Stark lied when he said, there are a lot of things Fury doesn't tell you.",
		"Is Thor back from his weekend trip to Asgard?",
		"I've received a resume. Travis wants to be on the Avengers Team as well. Tony?",
		"Does any of the avengers see flash? Great show FYI. Ohh Crap! DC Comics."
	];

var prepareAssignment = function(villianName, villianId) {
	var randomBuildUpSentence = buildUpSentences[random(0, buildUpSentences.length - 1)];
	var sentences = [randomBuildUpSentence, ]
	var randomNumber = random(0,1);
	if (randomNumber == 0) {
		return '[ASSIGNMENT - '+ sprintf('%19s', villianName + '] ') + villianName + ' is on the loose. ' + randomBuildUpSentence;
	} else {
		return '[ASSIGNMENT - ' + sprintf('%19s', villianName + '] ') + randomBuildUpSentence + ' ' + villianName + ' is on the loose.';
	}
	
};

var dangerRank = 25;
var searchVillians = function(dangerRank) {
	sleep.sleep(1);
	if (dangerRank == 0) {
		return;
	} else {
		Villian.findOne({
			dangerRank: dangerRank
		}).exec(function(err, villian) {
			dangerRank--;
			if (err || !villian) {
				console.log(err);
			} else {
				var assignmentMessage = prepareAssignment(villian.name, villian.id);
				logger.sendMessage(assignmentMessage, villian.id, function(lastConversationId) {
					assignment.sendMessage(assignmentMessage, lastConversationId, function() {
						searchVillians(dangerRank);
					});
				});
				
			}
		});
	}
};
searchVillians(dangerRank);