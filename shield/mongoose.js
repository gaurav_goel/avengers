var mongoose = require('mongoose');

var connect = function (environment) {
  var options = {
      server: {
        socketOptions: { keepAlive: 1 },
        poolSize: 10
      },
      forceServerObjectId: true
    };
  if (mongoose.connection.readyState == 0) {
    mongoose.connect('mongodb://localhost/avengers', options);
  }
  return mongoose;
};
// Error handler
mongoose.connection.on('error', function (err) {
  console.error('Error occurred while connecting to database - ' + err);
});
// Log success message on successful connection creation
mongoose.connection.once('open', function () {
  console.info('Successfully connected to the database.');
});
// Reconnect when closed
mongoose.connection.on('disconnected', function () {
  console.warn('Trying to reconnect to the ');
  connect();
});

////////////////////////////////
// Mongoose Schema
////////////////////////////////
var villianSchema = new mongoose.Schema({
    villianId: String
  , name: String
  , description: String
  , imageURL: String
  , dangerRank: Number
  , status: String
});

var Villian = mongoose.model('Villian', villianSchema);
////////////////////////////////
// Exports
////////////////////////////////
module.exports = {
  connect: function (env) {
    connect(env);
  },
  models: {
    Villian: Villian
  },
  isConnected: mongoose.connection.readyState == 0
};