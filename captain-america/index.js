var net = require('net');
var config = require('./config.js');
var imaging = require('imaging');
var color = require('cli-color');
var MY_NAME = config.MY_NAME;
var HOST = config.HOST;
var PORT = config.PORT;
var hulkClient = require('./hulk-client');
var ironManClient = require('./iron-man-client');
var logger = require('./logger');

var HULK_ID = 0;
var IRON_MAN_ID = 1;

var random = function(low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low);
}

var buildUpSentences = [
		"Please take this up dude.",
		"I was thinking there are multiple criterias to consider, before calling you a 'Grown Up'. LOL!",
		"Lets get down to work.",
		"Take this up please. Thor's cat is missing. Did you by any chance eat it up?"
	];

var tonyBuildUpSentences = [
	"Are you sober enough to take this up?",
	"Why is travis answering your phone dude?",
	"Buddy. START Industries going down on SENSEX, meanwhile please handle this.",
	"Work alert.",
	"Get this over with ASAP.",
	"Didn't you kill this monster? How did he manage to revive from the dead?",
	"You killed him last time. Right?"
];

var prepareAssignment = function(message, assignee) {
	var randomBuildUpSentence = assignee == 0 ? buildUpSentences[random(0, buildUpSentences.length - 1)]: tonyBuildUpSentences[random(0, tonyBuildUpSentences.length - 1)];
	var randomNumber = random(0,1);
	return randomBuildUpSentence + ' ' + message;
};

var assignmentCounter = {
	hulk: 0,
	tony: 0
}

net.createServer(function(sock) {    
    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        data = JSON.parse(data);
        console.log(data);

        var gotConversationId = function(conversationId) {
            // Write the data back to the socket, the client will receive it as data from the server
            var logMsg = "["+conversationId+"] " + (data.sender ? data.sender: 'Unknown')  +' said, "' + data.message + "";
            switch(data.sender) {
	            case 'Chitragupt': 
	                console.log(color.blue(logMsg));
	                break;
	            case 'S.H.I.E.L.D.':
	                console.log(color.white(logMsg));
	                sock.write("On it Mr. Fury.");

	                var assignee;
	                // var counterDiff = assignmentCounter.tony - assignmentCounter.hulk;
	                // if (counterDiff >= 0) {
	                // 	assignee = HULK_ID;
	                // } else {
	                	assignee = random(0,1);
	                // }

	                var assignmentSentence = prepareAssignment(data.message, assignee);
	                switch(assignee) {
	                	case HULK_ID:
	                		// HULK
	               			logger.sendMessage(assignmentSentence, conversationId, function() {
		                		hulkClient.sendMessage(assignmentSentence, conversationId, function() {
	                				assignmentCounter.hulk++;
	                				console.log('Avenger deployed.');
	                			})
	                		});
	                		break;
	                	case IRON_MAN_ID:
	                		// IRON MAN
	                		logger.sendMessage(assignmentSentence, conversationId, function() {
	                			ironManClient.sendMessage(assignmentSentence, conversationId, function() {
	                				assignmentCounter.tony++;
	                				console.log('Avenger deployed.');
	                			})
	                		});
	                		break;
	                }

	                break;
	            default:
	                console.log(color.red(log));
	        }
        };

		gotConversationId(data.conversationId);
        sock.destroy();
    });
    
    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
    	sock.destroy();
        // console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
}).listen(PORT, HOST);
console.log('Server listening on ' + HOST +':'+ PORT);
imaging.draw('./static/captainamerica.png', {char: '@'}, function(resp, status) {
        console.log(resp);
});